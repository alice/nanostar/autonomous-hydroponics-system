# -*- coding: utf-8 -*-
"""
Created on Mon May 11 07:43:34 2020

@author: gamil
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt #To visualize in graphs the data

#Import the dataset

bio = pd.read_csv("Biochar.csv")

##To analyze the dataset
print('Dataset information')
print(bio.info()) #Number of information, tyoe of information in each column objects

#Show the statistics of the dataset
print('Dataset description')
list(bio.columns.values.tolist()) 

