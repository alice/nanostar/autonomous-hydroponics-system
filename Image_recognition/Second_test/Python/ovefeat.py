# -*- coding: utf-8 -*-
"""
Created on Mon May 25 15:09:22 2020

@author: gamil
"""

def testEndPoints(self):
    batch_size = 5
    height, width = 231, 231
    num_classes = 1000
    with self.test_session():
      inputs = tf.random_uniform((batch_size, height, width, 3))
      _, end_points = overfeat.overfeat(inputs, num_classes)
      expected_names = ['overfeat/conv1',
                        'overfeat/pool1',
                        'overfeat/conv2',
                        'overfeat/pool2',
                        'overfeat/conv3',
                        'overfeat/conv4',
                        'overfeat/conv5',
                        'overfeat/pool5',
                        'overfeat/fc6',
                        'overfeat/fc7',
                        'overfeat/fc8'
                       ]
      self.assertSetEqual(set(end_points.keys()), set(expected_names)) 