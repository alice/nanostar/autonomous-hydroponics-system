# -*- coding: utf-8 -*-
"""
Created on Mon May 25 13:29:36 2020

@author: gamil
"""

import skimage.segmentation
from matplotlib import pyplot as plt

img2 = scipy.misc.imread("40.png", mode="L")
#Generates a finer-grained segmentation with small
segment_mask1 = skimage.segmentation.felzenszwalb(img2, scale=60)
segment_mask2 = skimage.segmentation.felzenszwalb(img2, scale=1000)

fig = plt.figure(figsize=(15, 10))
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)
ax1.imshow(segment_mask1); ax1.set_xlabel("k=56")
ax2.imshow(segment_mask2); ax2.set_xlabel("k=1000")
fig.suptitle("Felsenszwalb's efficient graph based image segmentation")
plt.tight_layout()
plt.show()