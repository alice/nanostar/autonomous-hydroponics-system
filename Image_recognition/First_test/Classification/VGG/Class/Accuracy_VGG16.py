pot
green_lizard
American_chameleon
mantis
picket_fence
['pot', 'green_lizard', 'American_chameleon', 'mantis', 'slug']
1	match
['pot', 'vase', 'paper_towel', 'rain_barrel', 'hook']
10	 
['pot', 'green_lizard', 'alligator_lizard', 'American_chameleon', 'whiptail']
2	match
['pot', 'green_lizard', 'American_chameleon', 'hook', 'cicada']
3	match
['cicada', 'green_lizard', 'American_chameleon', 'lacewing', 'pot']
4	match
['pot', 'American_chameleon', 'green_lizard', 'vase', 'picket_fence']
5	match
['American_chameleon', 'plow', 'green_lizard', 'barrow', 'pot']
6	match
['pot', 'American_chameleon', 'green_lizard', 'vase', 'rhinoceros_beetle']
7	match
['pot', 'American_chameleon', 'green_lizard', 'mantis', 'cucumber']
8	match
['pot', 'slug', 'American_chameleon', 'thunder_snake', 'alligator_lizard']
9	match


Predictions not taken into account: ['pot', 'slug', 'vase', 'paper_towel', 'rain_barrel', 'hook', 'cicada', 'picket_fence', 'plow', 'barrow', 'rhinoceros_beetle', 'thunder_snake']
Score: 24/50
Percentage of accuracy: 48.0 %
Computation time: 2.750596284866333 minutes 
Usage RAM: 922.431488 MB
