#!/usr/bin/env python3

"Compute accuracy for train images that contain green colors"
"Shows predictions which are rejected"

import os
from glob import glob
from vgg import total_time
from vgg import info_memory

green_list = '/home/gamilesca/Documents/VGG/green_list.txt'
predictions_dir = '/home/gamilesca/Documents/VGG/Class/'

# load green parts

gl = open( green_list )

green = gl.read()
green = green.split( "\n" )
green = { x: x for x in green }
#print(green)


files = glob( predictions_dir + '*.txt' )
files.sort()

acc_counter = 0
count_predictions = 0
rejected = []

def Convert(string): 
    li = list(string.split(" ")) 
    return li 

for f in files:
	
        # true class
        #print(f)
        basename = os.path.basename( f )
        #print(basename)
        true_class = basename.split( "." )[0]
        #print(true_class)
        #print true_class
	
        pf = open( f )
        predictions = pf.read()
        predictions = predictions.replace("\n", " ")
        predictions = Convert(predictions)
        predictions.remove('')
        print(predictions)

        predicted_class = " "
        for p in predictions:
                count_predictions += 1
                if p in green:
                        acc_counter += 1
                        predicted_class = 'match'
                else:
                        if p not in rejected:
                                rejected.append(p)

        print ("{}\t{}".format( true_class, predicted_class))


#print the accuracy of this angle
print("\n")
print("Predictions not taken into account:",rejected)		
print ("Score: {}/{}".format( acc_counter, count_predictions ))
accuracy = 100 * acc_counter / count_predictions
accuracy = round(accuracy, 2)
print ("Percentage of accuracy: {} %".format( accuracy ))
print("Computation time: %s minutes " % (total_time*10))
print("Usage RAM: {} MB".format(info_memory))
