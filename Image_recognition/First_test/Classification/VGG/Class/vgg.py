import time #To count the total time of computation
import psutil #To know the total RAM used
import os #Allows to interface with the underlying OS that Python is running on (Ubuntu 16.04)

#KERAS: Open-source neural-network library written in Python, enable fast experimentation with deep neural networks
from keras.applications.resnet50 import ResNet50 #Importing the recognition model to be used Op: AlexNet, VGG19, ResNet50 
from keras.preprocessing.image import load_img #Importing features to edit the image
from keras.preprocessing.image import img_to_array
from keras.applications.resnet50 import preprocess_input #Importing model features to compile the classification
from keras.applications.resnet50 import decode_predictions #Import this to decode the pro

#Start counting time and RAM consumption
process = psutil.Process(os.getpid())
start_time = time.time()
# load the model
model = ResNet50()

# load an image from file
image = load_img('/home/gamilesca/Documents/VGG/20200616_101458.jpg', target_size=(224, 224))

# convert the image pixels to a numpy array
image = img_to_array(image)

# reshape data for the model (depend on the model that have to be used)
image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))

# prepare the image for the VGG model
image = preprocess_input(image)

# predict the probability across all output classes
yhat = model.predict(image)

# convert the probabilities to class labels
label = decode_predictions(yhat)
#Print all the predictions in order to be compared
i = 0
predictions = []
while i<5:
    label1 = label[0][i][1]
    i+=1
    print(label1)

#Time of computing and RAM used during this process
total_time = (time.time() - start_time)/60
print(total_time)
info_memory = process.memory_info().rss*1e-6
print(info_memory)


