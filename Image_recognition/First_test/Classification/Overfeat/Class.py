#!/usr/bin/env python

"Set your paths below"

import os
import glob
import time
import psutil

#Checking the paths to the overfeat and the images
output_dir = '/home/gamilesca/Documents/Overfeat/Classification/'
overfeat = '/home/gamilesca/Downloads/overfeat/src/overfeat'
overfeat_data_dir = '/home/gamilesca/Downloads/overfeat/data/default/'


files = glob.glob('/home/gamilesca/Documents/Overfeat/Frenteraz/*.jpg')
files.sort()
i = 0
process = psutil.Process(os.getpid())
start_time = time.time()

#Running same command to each image file that will be detect and classified
for f in files:
    i+=1
    print (f)
    basename = os.path.basename(f)

    cmd = '{} -l -d {} {} -n 3 > {}{}.txt'.format( overfeat, overfeat_data_dir, f, output_dir, i)
    print (cmd)
    os.system(cmd)


total_time = (time.time() - start_time)/60
print("--- %s minutes ---" % (total_time))
info_memory = process.memory_info().rss
print(info_memory)  # in bytes


