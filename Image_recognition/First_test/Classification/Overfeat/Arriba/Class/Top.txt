['pot, flowerpot', 'plate', 'acorn squash']
1	match
['pot, flowerpot', 'vine snake', 'water snake']
10	match
['plate', 'hognose snake, puff adder, sand viper', 'cucumber, cuke']
2	match
['pot, flowerpot', 'stinkhorn, carrion fungus', 'agaric']
3	 
['pot, flowerpot', 'chocolate sauce, chocolate syrup', 'hognose snake, puff adder, sand viper']
4	match
['pot, flowerpot', 'vase', 'acorn squash']
5	match
['pot, flowerpot', 'vine snake', 'green snake, grass snake']
6	match
['chocolate sauce, chocolate syrup', 'pot, flowerpot', 'ringneck snake, ring-necked snake, ring snake']
7	 
['pot, flowerpot', 'acorn squash', 'coucal']
8	match
['stinkhorn, carrion fungus', 'pot, flowerpot', "yellow lady's slipper, yellow lady-slipper, Cypripedium calceolus, Cypripedium parviflorum"]
9	match


Predictions not taken into account: ['pot, flowerpot', 'plate', 'water snake', 'stinkhorn, carrion fungus', 'agaric', 'chocolate sauce, chocolate syrup', 'vase', 'green snake, grass snake', 'ringneck snake, ring-necked snake, ring snake', 'coucal']
Score: 9/30
Percentage of accuracy: 30.0 %
