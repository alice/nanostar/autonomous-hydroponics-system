#!/usr/bin/env python

import os #Allows to interface with the underlying OS that Python is running on (Ubuntu 16.04)
import glob #Retrieve files/pathnames matching a specified pattern
import time #To count the total time of computation
import psutil #To know the total RAM used

#Checking the paths to the overfeat and the images
output_dir = '/home/gamilesca/Documents/Overfeat/Classification/'
overfeat = '/home/gamilesca/Downloads/overfeat/src/overfeat'
overfeat_data_dir = '/home/gamilesca/Downloads/overfeat/data/default/'


#Assigning an specific path in order to loop in the folder "Front/potlevel" between the different images that will be tested
files = glob.glob('/home/gamilesca/Documents/Overfeat/Frenteraz/*.jpg')
files.sort()
i = 0

#Beginning to count the usage
process = psutil.Process(os.getpid())
start_time = time.time()

#Running same command to each image file that will be detect and classified
for f in files: #Loop within all the images in the folder
    i+=1
    print (f)
    basename = os.path.basename(f) #To extract the name of the image in the stack of files in the folder
    #
    cmd = '{} -l -d {} {} -n 3 > {}{}.txt'.format( overfeat, overfeat_data_dir, f, output_dir, i)
    print (cmd)
    os.system(cmd)
#Printing the time consumed and RAM used
total_time = (time.time() - start_time)/60
info_memory = process.memory_info().rss



