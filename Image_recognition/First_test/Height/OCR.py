# read image
img = cv2.imread('planta3.jpg')

##########Cropping the image to avoid other subjects detected############

image = img[355:1100,750:840].copy() #Cropping standard area where the plant is located
image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)

##################### Applying edition #############################

im_gray = gray(image) #Calling method to convert to gray
im_blur = blur(im_gray) #Calling method to apply blur to the image
im_thresh = threshold(im_blur) #Calling method to convert to gray

contours, _ = cv2.findContours(im_thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE) #Detect contours of image

##################### Text detection #################################

def contours_text(orig, img, contours):
 for cnt in contours:
    x, y, w, h = cv2.boundingRect(cnt) #Creating a bounding box to highlight text area
    rect = cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 255, 255), 2) # Drawing a rectangle on copied image
    cv2.imshow('cnt',rect) #Showing image with bounding box
    cv2.waitKey(60)
    cropped = orig[y:y + h, x:x + w] # Cropping the text block for giving input to OCR
    config = ('-1 eng --oem 1 --psm 13')# Apply OCR on the cropped image
    text = pytesseract.image_to_string(cropped, config=config) #Obtaining text from the image


##################### Text detection #################################
matches = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"] #Values that can appear in the scale portion of image
matchesl = [] #Actual values that will appear

for x in contents: #Searching in the words detected by pytesseract
  if any(word in x for word in matches): #Detecting if any value of the lists appear in the image
    matchesl.append(x) #Appeding this values

print("Predictions:", matchesl) #Printing all the values founded in the image

if "10" in contents: #Calculating heigth dizaines based on the visual support (only one o)
  index = contents.index("10") #Retrieving the index where 10 is founded
  unities = 11-index #Calculating unities of the heigth
  print("\nTotal heigth of the plants: {} cm".format(unities)) #Printing the total heigth
  print ("Plant in growing process : Between week 1 & 2") #Printing actual condition of the plant

if "20" in contents:
  index = contents.index("20") #Retrieving the index where 20 is founded
  unities = 11-index #Calculating unities of of the heigth
  h = 10+unities #Calculating total heigth based on the corresponding dizaine
  print("\nTotal heigth of the plants: {} cm".format(h))
  print("Plant in growing process : Between week 2 & 3")

if "30" in contents:
  index = contents.index("30") #Retrieving the index where 30 is founded
  unities = 11-index #Calculating unities of of the heigth
  h = 20+unities #Calculating total heigth based on the corresponding dizaine
  print("\nTotal heigth of the plants: (} cm".format(h))
  if h < 25: #Two conditions are meet in this stage of the plant
    print ("Plant is almost ready, only one week left!")
  else:
    print ("Plant in ready to be harvested!")
