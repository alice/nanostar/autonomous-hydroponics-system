from sklearn.metrics import confusion_matrix
from matplotlib import pyplot as plt

import csv
y_true = [2, 0, 2, 2, 0, 1]
y_pred = [0, 0, 2, 2, 0, 2]
confusion_matrix(y_true, y_pred)

def LoadData(locFile):
    with open(locFile) as csv_file:
        data = []
        label = []
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                data.append([row[0],row[1],row[2]])
                if row[3] == "1":
                    label.append(1)
                else:
                    label.append(0)
            line_count += 1
        return data, label

locFileTrain = "TRAIN_color.csv" #load data train
locFileTest = "TEST_color.csv" #load data test
train, trainLabel = LoadData(locFileTrain)
test, testLabel = LoadData(locFileTest)

train_predicted = [1, 0,1,1,0,1,1,1,1,1]

conf_mat= confusion_matrix(testLabel, train_predicted)
print('Confusion matrix:\n', conf_mat)

labels = ['Class 0', 'Class 1']
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(conf_mat, cmap=plt.cm.Blues)
fig.colorbar(cax)
ax.set_xticklabels([''] + labels)
ax.set_yticklabels([''] + labels)
plt.xlabel('Predicted')
plt.ylabel('Expected')
plt.show()


