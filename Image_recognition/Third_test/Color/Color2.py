# import the necessary packages

import numpy as np #All the OpenCV array structures are converted to and from Numpy arrays
import cv2 #OpenCV tool in the Image Processing and Management category of a tech stack
from glob import glob
#from Accuracy import plant_pot

predictions_dir = '/home/gabriela/Documents/Color/Test/180/'
#if plant_pot == 1:

# load the image
files = glob( predictions_dir + '*.jpg' )
files.sort()
features = []
for f in files:
	#print(f)
	############################ Reading image #############################
	image = cv2.imread(f)

	##################### Edition of the image #############################

	##################### Resizing the image ###############################
	height, width, depth = image.shape #Find image shape (tuple of height, width and channel of RGB)
	#print(height,width)
	scale = 0.2 #Find the best scale
	newW,newH = width*scale, height*scale #Apply the scale 
	#Resize of the image
	img_resize = cv2.resize(image,(int(newW),int(newH))) #Resize the image
	##########Cropping the image to avoid other subjects detected############
	newimg = img_resize[245:705,0:565].copy() #Cropping standard area where the plant is located
	#cv2.imshow("images", newimg)
	#cv2.waitKey(0)

	##################### Cleaning image wall ##############################

	# create NumPy arrays from the boundaries
	lower = np.array([0,55,31])  #-- Lower range in BGR
	upper = np.array([108,176,220])  #-- Upper range in BGR
	# find the colors within the specified boundaries and apply the mask
	mask = cv2.inRange(newimg, lower, upper) #Creating mask based on RGB boundaries
	output = cv2.bitwise_and(newimg, newimg, mask = mask) #Appying the mask to image
	# show the images
	#cv2.imshow("Image withouth background", np.hstack([newimg, output]))
	#cv2.waitKey(0)

	##################### Cleaning image pot ###############################

	#Same procedure as before
	lower2 = np.array([69,74,83])  
	upper2 = np.array([163,167,166])  
	mask2 = cv2.inRange(output, lower2, upper2) 
	output2 = cv2.bitwise_and(output, output, mask = mask2)
	#In this case we want to eliminate the mask applied to the image so we apply difference
	clean_image = cv2.subtract(output, output2)
	#cv2.imshow("Image of bad plant", np.hstack([output, clean_image]))
	#cv2.waitKey(0)

	##################### Extracting bad parts of the plant ########################

	#Same procedure as before
	lower3 = np.array([1,58,106])  
	upper3 = np.array([88,170,215]) 
	#In this case we want to extract only the bad parts of the plant in order to compare them 
	mask3 = cv2.inRange(clean_image, lower3, upper3)
	output3 = cv2.bitwise_and(clean_image, clean_image, mask = mask3)
	cv2.imshow("Part bad of the plant", np.hstack([clean_image, output3]))
	cv2.waitKey(1000)


	########Calculating number of pixels of the current image with a mask############


	##################### Converting to black/white scale ###########################

	def grayConversion(image):
	    grayValue = 0.07 * image[:,:,2] + 0.72 * image[:,:,1] + 0.21 * image[:,:,0]
	    gray_img = grayValue.astype(np.uint8)
	    return gray_img

	bad_gray = grayConversion(output3)

	###################### Count non-zero black values ###############################

	non_black = cv2.countNonZero(bad_gray) #Count the pixels of the image which are not black (dry area)
	size = bad_gray.shape[0]*bad_gray.shape[1] #Calculation of total pixels weight*height
	black = size - non_black #Counting pixels which are black (represents the area that is not dry)
	#print("Non-black pixels", non_black)
	#print("Black pixels", black)
	#print("Total number of pixels", size)
	percentage = ((non_black*100)/ size)
	#print ("\nPercentage of the plant which is dry/have unnaceptable conditions: {} %".format(round(percentage, 2)))
	features.append(percentage)
	#print(features)
	print(features)





