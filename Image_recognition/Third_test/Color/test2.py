import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df_train = pd.read_csv('/home/gabriela/Documents/Color/TRAIN_color.csv')
target_count = pd.value_counts(df_train.values.flatten())
print('Class 0:', target_count[0])
print('Class 1:', target_count[1])
print('Proportion:', (target_count[0] / target_count[1]), ': 1')
target_count.plot(kind='bar')
plt.show()
