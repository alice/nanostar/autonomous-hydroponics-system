Autonomous Hydroponic System
============================
Nanostar challenge and Master thesis project
--------------------------------------------
In this repository, you will find the work done in the framework of the nanostar challenge:
**"Automatic Vegetable Growing for Future Long-term Space Missions"**.

![Image](Design/Fig2.png "Hydroponic system")

* **Design**: in this folder you will find images of the 3d modeling of the hydroponic system.
* **Presentations**: this 2-years project had some 
* **Reports**: this two-year project has had review stages, the presentations of which are available here.
* **Sensors**: the arduino scripts needed to operate the sensors are stored here.
* **Image_recognition**: image processing and deep learning Algorithms to detect and analyze the condition of plants.
* **MDP**: planning under uncertainty algorithms to act on the system according to the condition of the plants.
* **Research_project_final_report**: final report of the project.

![Image](Research_project_final_report/Images/images.png "Samples from the dataset of arugula pictures.")

**Authors**: Gabriela CATALAN-MEDINA & Alois Adrien MENCIK-VON-ZEBINSKY
