#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Gabriela Catalan, Adrien Mencik

Actions code 
"""

NACTIONS = 9

#P0 = 0

P1 = 0
P2 = 1
P3 = 2
W1 = 3
W2 = 4
N1 = 5
N2 = 6
L1 = 7
L2 = 8

#P0:'PICK PLANT',

 
actionName = {
 P1:"GO NEXT PLANT",
 P2:"TROW AWAY PLANT",
 P3:"HARVEST PLANT",
 W1:"INCREASE WATER FLOW",
 W2:"DECREASE WATER FLOW",
 N1:"INCREASE NUTRIENTS",
 N2:"DECREASE NUTRIENTS",
 L1:"INCREASE LIGHT",
 L2:"DECREASE LIGHT"
 } 

