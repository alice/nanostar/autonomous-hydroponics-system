#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 04:56:29 2020

@author: Adri

Transition
"""

# This script generates the probability distribution
import numpy as NP;
from MDP_SOLVER import *;
from states import *;
from Actions import *;
from scipy.io import savemat; 




if __name__ == "__main__":# Fill the matrix P , such that P[i,j,k] is the 
    #probability of going from state i to state j if action k is taken.
    # All probabilities are 0 by default
    
    print ("Generating probability function")
    P = NP.zeros((NSTATES, NSTATES, NACTIONS)) #5,5,8
    
    P[0,4,0]=1
    P[0,4,1]=1 #inc light
    P[0,4,2]=1 #inc light
    P[0,1,4]=1 #inc light
    P[0,1,6]=1
    P[0,1,8]=1
    
    P[1,4,1]=1
    P[1,4,2]=1
    P[1,0,3]=1
    P[1,0,5]=1
    P[1,0,7]=1
    
    #P[2,0,0]=1
    
    P[0,0,7]=0.8 #inc light
    P[0,0,5]=0.8 #inc nut
    P[0,0,3]=0.8 #inc wat
    P[0,1,7]=0.05
    P[0,1,5]=0.05
    P[0,1,3]=0.05
    P[0,2,7]=0.15
    P[0,2,5]=0.15
    P[0,2,3]=0.15
    
    
    
    P[1,0,8]=0.05 #red light
    P[1,0,6]=0.05 #red nut
    P[1,0,4]=0.05 #red wat
    P[1,1,8]=0.9 
    P[1,1,6]=0.9
    P[1,1,4]=0.9
    P[1,3,8]=0.05
    P[1,3,6]=0.05
    P[1,3,4]=0.05
    P[1,4,0]=1 #go to plant
    
    P[2,4,:]=1 #throw away
    
    P[3,4,:]=1 #harvest
    
    P[4,4,:]=1 #harvest
    
    if (not is_P_OK(P)):
        print ("P is incorrect")
    else:
        print ("P is correct")
    print(P)        
    savemat('P.mat',{'P':P})
    print ("P saved correctly. Program finished")
