# -*- coding: utf-8 -*-
"""
@author: Gabriela Catalan, Adrien Mencik

Rewards 
"""

import numpy as NP;
from MDP_SOLVER import *;
from states import *;
from Actions import *;
from scipy.io import savemat

NPLANTS = 1

# This script computes the reward function and saves it to file R.npy 

DEAD_PUNISHMENT = -3
NON_GROWING_PUNISHMENT = -0.5
WATER_EXPENSE = -0.1
NUTRIENT_EXPENSE = -0.1
LIGHT_EXPENSE = -0.1

WATER_REWARD = 0.1
NUTRIENT_REWARD = 0.1
LIGHT_REWARD = 0.1
HARVEST_REWARD = 3 

ltresh = 0.1
htresh=1 

if __name__ == "__main__":
    R = NP.zeros((NSTATES, NSTATES, NACTIONS))
    print ("Generating reward function")
 
    
    R[0,0,7]=-0.1#inc light
    R[0,0,5]=-0.1 #inc nut
    R[0,0,3]=-0.1 #inc wat
    R[0,1,7]=-0.1
    R[0,1,5]=-0.1
    R[0,1,3]=-0.1
    R[0,2,7]=-0.1
    R[0,2,5]=-0.1
    R[0,2,7]=-0.1
    
    R[1,0,8]=+0.1 #red light
    R[1,0,6]=+0.1 #red nut
    R[1,2,4]=+0.1 #red wat
    R[1,1,8]=+0.1 
    R[1,1,6]=+0.1
    R[1,1,4]=+0.1
    R[1,3,8]=+0.1
    R[1,3,6]=+0.1
    R[1,3,4]=+0.1
    R[1,4,0]=0 #go to plant
    
    R[2,4,1]=-5 #throw away
    
    R[3,4,2]=10 #harvest
    
savemat("R.mat",{"R":R})
print ("R saved correctly. Program finished")



"""   
    for nstate_init in range(NSTATES):
        (green_i,height_i, water_i, nut_i, temp_i) = decode_state(nstate_init)
        nw = 0
         
            for nstate_next in range(NSTATES):
            (green_n,height_n, water_n, nut_n, temp_n) = decode_state(nstate_next)
            # Punish dead
            if (green <= ltresh): #the plant is not green enough
                R[nstate_init,nstate_next,:] = DEAD_PUNISHMENT
            else if (green >= htresh): #the plant is ready to be harvested
                R[nstate_init,nstate_next,:] = HARVEST_REWARD
            else:
                nwd = 0
        for i in xrange(5):
            if green_dest[i]>water_dest[i]:
                nwd += 1
        for action in xrange(NACTIONS):
            if (action > 3):# Punish the water expense
                R[nstate_ori,nstate_dest,action] =- WATER_EXPENSE 
# We get a punishment for every non watered fire that we leave behind

if ((action == P2 and green[NORTH]==1 and water_ori[NORTH]==0) or
    (action == FEA and fire_ori[EAST]==1 and water_ori[EAST]==0) or
    (action == FSA and fire_ori[SOUTH]==1 and water_ori[SOUTH]==0) or
    (action == FWA and fire_ori[WEST]==1 and water_ori[WEST]==0)):
    
    R[nstate_ori,nstate_dest,action] -= NON_GROWING_PUNISHMENT
    
    else:
        R[nstate_ori,nstate_dest,action] += nw * NON_GROWN_PUNISHMENT 

#print "From:"                    
#show_state_n(nstate_ori)
#                    print "To:"
#                    show_state_n(nstate_dest)
#                    print "Action:"
#                    print action
#                    print "R=",R[nstate_ori,nstate_dest,action]
print
"""