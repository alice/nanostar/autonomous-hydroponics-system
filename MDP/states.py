#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Gabriela Catalan, Adrien Mencik

States code
"""
import numpy as NP

"""
possible states: 
    level green > htresh
    level green >dog*a & <htresh
    level green <dog*a & >ltresh
    level green <ltresh``
    end
    + nutrients, water, temp levels. 
"""

NPLANTS = 1
NSTATES = 5

def decode_greeness(x):

    #code provided by gaby
    return

#def decode_height(x):
     
     

def decode_water(x):
    #water sens
    
    return

def decode_nutrients(x):
    #ph sensor
     
    return

def decode_temp(x):
    #temp sensor
     
    return
    
def decode_state(x):
   
    return (decode_greeness(x), decode_water(x), decode_nutrients(x), 
            decode_temp(x))




def encode_state(green, height, water, nut, temp):
    #encode states into a vector
    x = NP.zeros(4)
    
    #x(0) = green
    #x(2) = height
    #x(1) = water
    #x(2) = nut
    #x(3) = temp 
    
    return x

def show_state_n(n):
    (green, water, nut, temp) = decode_state(n)
    print(green, water, nut, temp)