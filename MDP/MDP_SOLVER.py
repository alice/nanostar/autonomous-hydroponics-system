#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Gabriela Catalan, Adrien Mencik

MDP solver
"""
import numpy as NP;

# checks if the probability matrix is normalized.

def is_P_OK(P):
    ns = P.shape[0]
    na = P.shape[2]
    eps = 0.0000001
    for state in range(ns):
        for action in range(na):
            s = NP.sum(P[state,:,action])
            #print s
            if (abs(s-1) > eps):
                return False
    return True 

# Value iteration resolution

def NP_value_iteration(P,R,gamma,theta):
    
    print ("R\n",R)
    print ("P\n",P)
    s = P.shape[0]                      
    delta = 2*theta
    V = NP.zeros((s,1))         
    NV = NP.zeros((s,1))
    i=0
    while delta > theta:
        print ("-----------------")
        print (" Iteration number",i)
        print ("-----------------")
        
        a = NP.sum(P*(R+gamma*V),axis=1) #value function iteration
        NV = (NP.amax(a,axis=1)).reshape(s,1) #new value of V, 
        #finding the max value for all states
        
        delta = NP.max(NP.abs(V-NV))
        V[:]=NV[:]
        i+=1
        print ("Delta: ", delta)
    return V

# Computes a policy from a value function.
def NP_computes_policy(P,R,gamma,V):
       
    a = NP.sum(P*(R+gamma*V),axis=1) #value function iteration
    return NP.argmax(a,axis=1) 








if __name__=="__main__":
    
    #easy example. 
    
    # Usage example:
    # Step 1 : Enumerate all the possible states (say m) from 0 to m - 1
    # Three states 0,1,2
    # Step 2 : Enumerate all the possible actions (say n) from 0 to n - 1
    # Two actions 0,1
    # Step 3 : Fill a real mxmxn matrix P , such that P[i,j,k] is the probability of going from state i to state j if action k is taken.
    P = NP.zeros((3,3,2))
    P[0,0,0]=0.9
    P[0,0,1]=0
    P[0,1,0]=0.05
    P[0,1,1]=1
    P[0,2,0]=0.05
    P[0,2,1]=0
    P[1,0,0]=0
    P[1,0,1]=0
    P[1,1,0]=1
    P[1,1,1]=0
    P[1,2,0]=0
    P[1,2,1]=1
    P[2,0,0]=0
    P[2,0,1]=1
    P[2,1,0]=0
    P[2,1,1]=0
    P[2,2,0]=1
    P[2,2,1]=0
    
    if (not is_P_OK(P)):
        print ("P is incorrect")
    else:
        print ("P is correct")
    # Step 4 : Fill a real mxmxn matrix R , such that R[i,j,k] is the reward of applying action k in state i and reaching state j afterwards.
    R = NP.zeros((3,3,2))
    R[:,2,:] = 1
    # Now run value iteration
    V = NP_value_iteration(P,R,0.3,0.000000000000001)
    print ("Value:", V)
    print(P)
    # and find a policy with this value
    policy = NP_computes_policy(P,R,0.3,V)
    print ("Policy:", policy)
    print ("Finished MDP example" )

