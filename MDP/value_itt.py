#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Gabriela Catalan, Adrien Mencik

value iteration
"""

# Runs value iteration on matrices stored as P.mat and R.mat 
#and saves the policy as policy.mat

import numpy as NP;
from states import *;
from Actions import *;
from MDP_SOLVER import *;
from scipy.io import savemat,loadmat;

gamma = 0.6
theta = 0.00001

P=loadmat("P.mat")["P"]

if (is_P_OK(P)):
        print ("P is correct")
else:
        print ("P is incorrect")
        
R=loadmat("R.mat")["R"]

V = NP_value_iteration(P,R,gamma,theta)
print("V is:", V)
policy = NP_computes_policy(P,R,gamma,V)
print("The policy is: ", policy)
savemat("policy.mat",{"policy":policy}) 