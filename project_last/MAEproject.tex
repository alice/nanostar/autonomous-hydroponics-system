\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc}    
\usepackage{hyperref}
\usepackage{url}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{tikz}
\geometry{hmargin=2.5cm,vmargin=1.5cm}


\title{Automatic vegetable growing\\
for future long-term space missions}
\author{
\hspace{1.2cm} \includegraphics[scale=0.15]{mars} \hspace{0.4cm}
Nicolas Drougard \hspace{0.4cm} \includegraphics[scale=0.6]{nanostarLogo} \\
Thibault Gateau\\
Caroline Chanel\\
Department of Aerospace vehicles design and control (DCAS)
}
\date{\today}
% NANOSTAR CHALLENGE: greenhouse on Mars
\usepackage{graphicx} % Required for including images
\graphicspath{{figures/}} % Directory in which figures are stored

\usepackage{amsmath} % For typesetting math
\usepackage{amssymb} % Adds new symbols to be used in math mode

\usepackage{booktabs} % Top and bottom rules for tables
\usepackage{enumitem} % Used to reduce itemize/enumerate spacing
\usepackage{palatino} % Use the Palatino font
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{soul}
\usepackage{color}

\usepackage{multicol} % Required for multiple columns
\setlength{\columnsep}{1.5em} % Slightly increase the space between columns
\setlength{\columnseprule}{0mm} % No horizontal rule between columns

\usepackage{tikz} % Required for flow chart
\usetikzlibrary{shapes,arrows} % Tikz libraries required for the flow chart in the template
\usetikzlibrary{decorations.pathmorphing}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[english,ruled,vlined,linesnumbered]{algorithm2e}
\usepackage{fancybox}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage{array}

\usepackage{graphics} %% TOUT NEW!!
\usepackage{pgfplots} %% TOUT NEW!!  virer si pbm
\usepackage{url}
\usepackage{fancybox}
\newcommand{\paren}[1]{\left( \left. #1 \right. \right)} 
\newcommand{\croch}[1]{\left[ \left. #1 \right. \right]} 
\newcommand{\set}[1]{\left\{ \left. #1 \right. \right\}}
\newcommand{\absj}[1]{\left\lvert #1 \right\rvert} 
\newcommand{\normsup}[1]{\left\lVert #1 \right\rVert_{\infty}} 
\newcommand{\R}{\mathbb{R}} % réels
\newcommand{\N}{\mathbb{N}} %entiers naturels
\newcommand{\E}{\mathbb{E}} % espérance
\RequirePackage{dsfont}
\newcommand{\1}{\mathds{1}} % incatrice
\newcommand{\un}{\mathds{1}} % indicatrice bis
%\DeclareMathOperator{\argmin}{argmin} 
%\DeclareMathOperator{\argmax}{argmax} 
\newcommand{\sachant}{\, \right| \left. \,} % pour l'esp\'erance conditionnelle
\newcommand{\States}{\mathcal{S}} % espace d'états
\newcommand{\Actions}{\mathcal{A}} % espace des actions
\newcommand{\Observ}{\mathcal{O}} % espace des observations
\newcommand{\echL}{\mathcal{L}} % echelle qualitative
\newcommand{\guil}[1]{``#1''} 
\newcommand{\Prob}{\mathbb{P}} %probabilite
\newcommand{\prob}{\mathbf{p}} %probabilite
\newcommand{\telque}{\mbox{ t.q. }} 
%\DeclareMathOperator{\card}{Card} % cardinal
\newcommand{\poss}{\pi} % possibilité
\newcommand{\nec}{\eta} % necessité
\newcommand{\Poss}{\Pi} % mesure de possibilité
\newcommand{\Nec}{\mathcal{N}} % mesure de nécessité
\newcommand{\bel}{\mathbf{b}} % belief probabiliste
\newcommand{\bp}{\beta} % belief possibiliste
\newcommand{\Bel}{\mathcal{B}el} % croyance
\newcommand{\Pl}{\mathcal{P}l} % plausibilité
\newcommand{\X}{\mathcal{X}} % ensemble X
\newcommand{\Y}{\mathcal{Y}} % ensemble Y
\newcommand{\Bellman}{\mathcal{B}} % opérateur de prog dyn
\newcommand{\FB}{\mathcal{F}_{B}}
\newcommand{\aaa}{\mbox{a}} 
\newcommand{\ooo}{\mbox{o}}
\newcommand{\bao}{\beta^{a,o'}}
\newtheorem{thm}{Theorem}
\newtheorem{prp}{Proposition}
\newcommand{\nico}[1]{}
\newcommand{\equalto}[2]{\underset{\overset{\verteq}{#2}}{#1}}
\newcommand{\verteq}{\rotatebox{90}{$\,=$}}


\begin{document}
\maketitle
\definecolor{ggreen}{RGB}{0.0,153,0.0}
\definecolor{vviolet}{RGB}{180,0,180}


Key words: \textit{Bioregenerative Life Support System,
Precision Agriculture, Robotics, Machine Learning,
Computer Vision, Planning under Uncertainty.}

\section{Introduction}
When space missions include humans,
it is necessary to set up a life support system (LSS).
Future space missions (e.g. colonization of Mars)
are expected to involve longer distances and durations.
For such missions, resupply from Earth is too expensive
(or even impossible for a one-way trip)
both in terms of energy and time,
and it therefore becomes necessary to design
bioregenerative LSS \cite{fu2016establish}.

This is why the implementation of such systems
is of interest to ESA: \textit{``For more than 30 years,
the European Space Agency (i.e. ESA)
is active in the field of regenerative life support system.
MELiSSA (Micro-Ecological Life Support System Alternative)
is the European project of circular life support system.
It was established to gain knowledge on regenerative system,
aiming to the highest degree of autonomy
and consequently to produce food,
water and oxygen from mission wastes.''}%
\footnote{\url{www.melissafoundation.org/}}.

Growing plants in space,
in addition to providing fresh food for astronauts,
makes it possible to mitigate
\textit{\enquote{stress effects from long-duration space travel}}
\cite{kyriacou2017microgreens}.
This is also a subject of interest since
research does not exclude that
\href{https://www.acs.org/content/acs/en/education/%
resources/highschool/chemmatters/past-issues/2016-2017/%
april-2017/growing-green-on-the-red-planet.html}%
{it is possible to grow plants on the Moon and Mars}
\cite{wamelink2014can}.
For the European Space Agency (ESA), 
\textit{\enquote{cultivating plants for food
was a significant step in the history of mankind.
Growing plants for food in space and on other planets
will be necessary for exploration of our Universe}}%
\footnote{\url{https://www.esa.int/Science_Exploration/%
Human_and_Robotic_Exploration/Research/Plants}}.


Space food has always been brought from earth%
\footnote{\url{https://airandspace.si.edu/exhibitions/%
apollo-to-the-moon/online/astronaut-life/food-in-space.cfm}}
but for more than twenty years
some plant growth systems for space have been set up 
and tested in various conditions 
\cite{zabel2016review,wheeler2017agriculture, carillo2020challenges}%
\footnote{see also \url{www.astrobotany.com}.},
for instance SVET (on Mir station, 1997) \cite{ivanova2001first}
and Lada (on the ISS, 2002) \cite{bingham2002lada}.

More recently, NASA set up the Vegetable Production System \cite{levine2016vegetable}
(Veggie, 2014\footnote{\url{https://www.nasa.gov/content/growing-plants-in-space}})
that produced the \href{https://www.youtube.com/watch?v=y9aR2-7sOjg}%
{first space-grown salad eaten by the astronauts}%
\footnote{\url{https://www.nasa.gov/mission_pages/%
station/research/news/meals_ready_to_eat}} 
(see Figure \ref{salad}).
In 2017, it was supplemented by
the Advanced Plant Habitat (APH), that
\textit{\enquote{is the largest growth chamber aboard the orbiting laboratory.
Roughly the size of a mini-fridge,
the habitat is designed to test
which growth conditions plants prefer in space
and provides specimens a larger root and shoot area.}}%
\footnote{\url{https://www.nasa.gov/mission_pages/%
station/research/Giving_Roots_and_Shoots_Their_Space_APH}}.
%NASA is also interested in hydroponic methods of growing plants in space:
%\textit{\enquote{Working with NASA, a company developed an analyzer 
%that provides real-time detection of nutrients,
%organics, and metals in water.}}%
%\footnote{\url{https://www.nasa.gov/offices/%
%oct/home/tech_life_asa_analytics.html}}.

The european project EDEN ISS 
\cite{zabel2015introducing}\footnote{\url{https://eden-iss.net/}}
has also studied, until 2019,
systems architecture for cultivating plants in
closed-loop systems for applications on earth and in space.
This project led for instance to
greenhouse design concept for the Moon and Mars \cite{imhof2019greenhouse}.
%\textit{``MELiSSA (Micro Ecological Life Support System Alternative)
%is an international collaborative effort of 15 partners
%led by European Space Agency (ESA)
%focused on the development
%of a Regenerative Life Support System
%to support long-term Space Missions.
%\textit{``The goals of MELiSSA are the production of food,
%recovery of water and regeneration of the atmosphere,
%with a concomitant use of wastes,
%i.e. CO2 and organic wastes,
%using light as a source of energy.''}
%\footnote{\url{https://www.melissafoundation.org%
%/page/melissa-pilot-plant}}.
Within the framework of the MELiSSA project,  
prototypes have recently been developed
(e.g. for cultivation of tuberous plants \cite{paradiso2020design}
in the ESA Project ``Precursor of Food Production Unit'')
and many research works have been carried out
(e.g. on hydroponic systems \cite{palermo2012hydroponic} 
or potatoes in controlled environments \cite{paradiso2019growth}).

\begin{figure}
\center
\includegraphics[scale=2]{eight_col_spacesalad.jpg}
\caption{First space-grown salad eaten by astronauts in 2015 on the ISS.}
\label{salad}
\end{figure}

\section{Automatic vegetable growing systems}
During most of the space missions,
human time and attention are very precious.
For instance, in the International Space Station (ISS),
it is preferable not to assign additional tasks
that are not related to ongoing experiments:
\textit{``At any given time on board the space station,
a large array of different experiments are underway
within a wide range of disciplines.''}%
\footnote{\url{www.nasa.gov/mission_pages/%
station/research/experiments_category}}.

Plant cultivation would be a long and repetitive daily task
that would increase too much the workload of the human team involved.
It is therefore interesting to design systems for growing plants
with automated monitoring and interventions.

In the context of the Nanostar challenge
and with the support of \textit{Innovspace by ISAE-SUPAERO},
prototypes of such automated systems
equiped with sensors (e.g. camera) have been studied
and are about to be implemented (see Figure \ref{systems}).
The goal of this initiative is to pave the way towards
autonomous plant growing system capable of analyzing 
and reacting to the growing process 
in order to quickly obtain healthy plants 
while optimally use space 
and resources (nutrient, water, etc.).

\begin{figure}
\center
\includegraphics[scale=0.165]{Fig2.png}
\includegraphics[scale=0.283]{farmbot.jpg}
\caption{Plant cultivation systems dedicated to the project:
on the left, a hydroponic system designed by a team of MAE students.
On the right, the \textit{Farmbot} robotic system.}
\label{systems}
\end{figure}

The aim of this master project is to use
the controlled environments we have available
to evaluate cultivation strategies.
By cultivation/growing strategy,
we mean a function of the information coming from the sensors
and of the current state of the system (plant \& robot)
that returns the actions performed by the robot over time
(that are translated into controls to actuators).
This project will collect a database on the growth of vegetables 
with context (history of sensors data, strategy used)
and will allow the optimization of automated cultivation strategies. 
The strategy optimization will be based on artificial intelligence: 
machine learning \& computer vision (e.g. \url{scikit-learn.org}), 
image processing (e.g. \url{opencv.org}) as well as 
Planning under Uncertainty \cite{shani2013survey} (see Figure \ref{POMDP}).

This project will be carried out in the following stages:
\begin{itemize}
\item Bibliographical reseach on vegetable monitoring with sensors and on automated growing strategies.
\item Study and use of the prototypes: the MAE hydroponic system and the \textit{Farmbot} system (\url{https://farm.bot/}). 
\item Development of graphical interfaces, implementation of the data collection and the cultivation strategy deployment.
\item \href{https://pomegranate.readthedocs.io/en/latest/index.html}{Learning} of the system dynamics and 
\href{https://bigbird.comp.nus.edu.sg/pmwiki/farm/appl/index.php?n=Main.PomdpXDocumentation}{optimization}
of growing strategies for these systems.
\end{itemize}




\begin{figure}
\center
\begin{tikzpicture}[scale=1,transform shape]
\tikzstyle{mvertex}=[circle,fill=blue!30,minimum size=30pt,inner sep=0pt]
\tikzstyle{overtex}=[circle,fill=ggreen!55,minimum size=30pt,inner sep=0pt]
\tikzstyle{avertex}=[fill=vviolet!40,minimum size=25pt,inner sep=0pt]
\def\stx{3}
\def\ax{1.5}
\def\sty{0}
\def\dist{6}
\def\dfacto{3.5}
\def\disty{1}
\def\degProbo{320}
\def\rewx{6}
\def\rewy{-2.5}
\def\hx{9}
\def\hy{-4}


\node[mvertex] (state1t) at (\stx,\sty) {$s_t$};
\node[avertex] (action) at (\ax+\dist,\sty-\disty) {$a_t$};

\node[mvertex] (state2t) at (\stx+\dist,0) {$s_{t+1}$};
\tikzstyle{bvertex}=[circle, draw=black,minimum size=30pt,inner sep=0pt]
%1->2
\draw[->,>=latex] (state1t) -- node [right=6pt, below=1pt] {$\textbf{p} \paren{ s_{t+1} \sachant s_{t},  a_t }$}  (state2t);
\draw[->,>=latex] (action) -- (state2t);

\node[overtex] (hobserv2) at (\stx+\stx-\ax+\dist,\sty-\dfacto*\disty) {$o_{t+1}$};
\draw[->,>=latex] (state2t) to (hobserv2);
\draw[->,>=latex] (action) -- (hobserv2);

\tikzstyle{rvertex}=[fill=yellow!60,minimum size=15pt,minimum width=42pt,inner sep=0pt]
\node[rvertex] (Rtbis) at (\rewx+0.3,\rewy-0.9) {$\equalto{\mbox{\textbf{reward}}}{\mbox{\textbf{production}}}$};
\node[rvertex] (Rt) at (\rewx,\rewy) {$r(s_t,a_t)$};
\draw[->,decorate,decoration={snake,amplitude=.4mm,segment length=2mm,post length=1mm}] (state1t) -- (Rt); 
\draw[->,decorate,decoration={snake,amplitude=.4mm,segment length=2mm,post length=1mm}] (action) -- (Rt); 

\node (state0) at (\stx-2,0) {};
\node (state3) at (\stx+\dist+2,0) {};
\node (state3bis) at (13,-0.5) {};
\node (obs3) at (13,-2.3) {};
\draw[->,>=latex,dashed] (state0) -- (state1t);
\draw[->,>=latex,dashed] (state2t) --  (state3);

\node[avertex] (action0) at (\ax,\sty-\disty) {$a_{t-1}$};
\draw[->,>=latex] (action0) -- (state1t);

\node[overtex] (hobserv1) at (\stx+\stx-\ax,\sty-\dfacto*\disty) {$o_{t}$};
\draw[->,>=latex] (state1t) to (hobserv1);
\draw[->,>=latex] (action0) -- node[above=1pt,rotate=\degProbo] {$\textbf{p} \paren{ o_{t} \sachant s_t,a_{t-1} }$}  (hobserv1);

\definecolor{lightblue}{rgb}{0.145,0.6666,1} % Defines the color used for content box headers
\node at (13,0.3) {{\color{blue!60} \large \textbf{State of the plants}}};
\node at (13,-0.2) {{\color{blue!60} \textbf{(partially observable)}}};
\node at (12.5,\sty-\disty) {{\color{violet!70} \large \textbf{Growing strategy}}};
\node at (12.5,\sty-\disty-0.5) {{\color{violet!70}  \textbf{(water, nutrients, light,}}};
\node at (12.5,\sty-\disty-1) {{\color{violet!70} \textbf{temperature, pruning, etc.)}}};
\node at (\stx*0.5+\ax*0.5,\sty-\disty*\dfacto) {\color{ggreen} \large \textbf{Observations:}};

\node[inner sep=0.2cm] (ET) at (\stx*0.5+\ax*0.5,\sty-\disty*\dfacto-0.6) {{\color{ggreen} \textbf{Video streams}}};
\node[inner sep=0.2cm] (ECG) at (\stx+0.85,\sty-\disty*\dfacto-1.1) {{\color{ggreen} \textbf{Sensors (humidity, light, temperature, etc.)}}};
\draw[->,>=latex,color=ggreen,thick] (ET) to (\hx-0.2,\hy-0.2);
\draw[->,>=latex,color=ggreen,thick] (ECG) to (\hx-0.2,\hy-0.6);
%\draw[->,>=latex,color=ggreen,thick] (HAI) to (\hx-0.2,\hy-0.5);
\node at (\hx,\hy) {\includegraphics[scale=0.2]{plant}};

\end{tikzpicture}
\caption{Partially Observable Markov Decision Process model 
for automatic vegetable growing system.
The actions of the robot are denoted by $a_t$,
streams from sensors $o_t$ and system state $s_t$. }
\label{POMDP}
\end{figure}


\bibliographystyle{plain}
\bibliography{mybibfile}

\end{document}


%\begin{abstract}
%growing fresh food in space to support astronauts on long-term missions.
%https://en.wikipedia.org/wiki/Plants_in_space
%https://en.wikipedia.org/wiki/Space_farming
%https://www.nasa.gov/mission_pages/station/research/experiments/explorer/Investigation.html?#id=1159
%https://www.google.com/search?hs=zur&channel=fs&q=space+agriculture&tbm=isch&source=univ&client=ubuntu&sa=X&ved=2ahUKEwiGwdLZjablAhWHA2MBHZBbCWoQ7Al6BAgFECQ#imgrc=Q3tt4G_rfOw8nM:
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.agriculture.mdpcdn.com%2Fsites%2Fdefault%2Ffiles%2Fimage%2F2017%2F10%2F09%2Ffarming-space.jpg&imgrefurl=https%3A%2F%2Fwww.agriculture.com%2Ftechnology%2Fcrop-management%2Ffarming-in-space&docid=40p4PZSwqImmTM&tbnid=TbXTRbaHjBelYM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhIKAAwAA..i&w=1709&h=1620&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhIKAAwAA&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdn.hswstatic.com%2Fgif%2Fspace-farming-1.jpg&imgrefurl=https%3A%2F%2Fscience.howstuffworks.com%2Fspace-farming.htm&docid=6WZlYV_Ww_ERIM&tbnid=mpldOt8_Nngv5M%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhJKAEwAQ..i&w=400&h=271&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhJKAEwAQ&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=http%3A%2F%2Fwww.freakingnews.com%2Fpictures%2F23000%2FSpace-Agriculture-Growing-Corn--23119.jpg&imgrefurl=http%3A%2F%2Fwww.freakingnews.com%2FSpace-Agriculture-Growing-Corn-Pictures-28489.asp&docid=MOTXeMph1Tb5yM&tbnid=Q3tt4G_rfOw8nM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhKKAIwAg..i&w=800&h=608&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhKKAIwAg&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=http%3A%2F%2Fwww.freakingnews.com%2Fpictures%2F23000%2FSpace-Agriculture-Growing-Corn--23119.jpg&imgrefurl=http%3A%2F%2Fwww.freakingnews.com%2FSpace-Agriculture-Growing-Corn-Pictures-28489.asp&docid=MOTXeMph1Tb5yM&tbnid=Q3tt4G_rfOw8nM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhKKAIwAg..i&w=800&h=608&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhKKAIwAg&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.syfy.com%2Fsites%2Fsyfy%2Ffiles%2F2017%2F06%2Fjay-wong-space-farm-on-moon-accomplished.jpg&imgrefurl=https%3A%2F%2Fwww.syfy.com%2Fsyfywire%2Fspace-the-final-frontier-of-farming&docid=hrSIsHpFhMHxgM&tbnid=j642OJHS2YDwxM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhLKAMwAw..i&w=1920&h=1358&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhLKAMwAw&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Ficdn2.digitaltrends.com%2Fimage%2Fdigitaltrends%2Fthe-martian-movie-006.jpg&imgrefurl=https%3A%2F%2Fwww.digitaltrends.com%2Fcool-tech%2Fspace-farming-hormone%2F&docid=fa5d1Jw7TzXydM&tbnid=BnoELwMEVjBWYM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhMKAQwBA..i&w=1500&h=844&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhMKAQwBA&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fs.hswstatic.com%2Fgif%2Fstufftoblowyourmind-23-2013-05-space-farm.jpg&imgrefurl=https%3A%2F%2Fwww.stufftoblowyourmind.com%2Fblogs%2Fleaf-cutter-ants-and-the-future-of-space-agriculture.htm&docid=LO3pj4m_BFRJiM&tbnid=niPI2n29SchjjM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhNKAUwBQ..i&w=360&h=240&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhNKAUwBQ&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=http%3A%2F%2Fresizer.shared.arcpublishing.com%2FrgvOX0TwlqAsOMCDp9wuDwBKuPg%3D%2Farc-anglerfish-arc2-prod-bonnier%2Fpublic%2FFZD6JLWMCZDYNSD4VFWNO3777Y.jpg&imgrefurl=https%3A%2F%2Fwww.popsci.com%2Fnasa-space-agriculture-farm-mars%2F&docid=JcY9jWpdqe3a-M&tbnid=Krsi1BJLA70YmM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhPKAcwBw..i&w=946&h=710&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhPKAcwBw&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fexternal-preview.redd.it%2FcR0Gj7ByCtxOQXj5kqk_RQzLt9RIqjC0Wm-m0BiBnKw.jpg%3Fauto%3Dwebp%26s%3D2bf1574fccfd538dcf80a4ee54cb06f311b02abb&imgrefurl=https%3A%2F%2Fwww.reddit.com%2Fr%2FIsaacArthur%2Fcomments%2F722044%2Fagriculture_tubes_in_space_nasa_concept_art%2F&docid=kITTPWPySjFreM&tbnid=3ApysI8GHXE9zM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhQKAgwCA..i&w=1920&h=1294&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhQKAgwCA&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdn.theatlantic.com%2Fassets%2Fmedia%2Fimg%2Fmt%2F2019%2F04%2FRTS192JY-1%2Flead_720_405.jpg%3Fmod%3D1556383913&imgrefurl=https%3A%2F%2Fwww.theatlantic.com%2Fscience%2Farchive%2F2019%2F04%2Fcan-humans-farm-plants-mars-and-moon%2F588145%2F&docid=twEWdUU5s2pWJM&tbnid=Io92rKeXI3rRzM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhSKAowCg..i&w=720&h=405&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhSKAowCg&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.elevenrivers.org%2Fenglish%2Fwp-content%2Fuploads%2F2018%2F05%2F05-precision-agriculture-from-the-space-1000x480.jpg&imgrefurl=https%3A%2F%2Fwww.elevenrivers.org%2Fboost-to-precision-agriculture-from-space%2F&docid=Un7WzRkkEZyNCM&tbnid=cjKvmaWZHNJl4M%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhTKAswCw..i&w=1000&h=480&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhTKAswCw&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=http%3A%2F%2Fwww.louisvillegrows.org%2Fwp-content%2Fuploads%2F2014%2F03%2F0412171335a-1.jpg&imgrefurl=http%3A%2F%2Fwww.louisvillegrows.org%2Furban-agriculture%2F&docid=PxFj9NwR0ylS3M&tbnid=wGPuMvBxJM377M%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhVKA0wDQ..i&w=2336&h=1612&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhVKA0wDQ&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fi.cbc.ca%2F1.4058320.1522435033!%2FfileImage%2FhttpImage%2Fimage.jpg_gen%2Fderivatives%2F16x9_780%2Fhydroponics.jpg&imgrefurl=https%3A%2F%2Fwww.cbc.ca%2Fnews%2Ftechnology%2Fgrowing-food-space-1.4058203&docid=9C18uRUvDO2QVM&tbnid=4VmfcvkpNr-wvM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhqKBQwFA..i&w=780&h=439&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhqKBQwFA&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fstatic.latribune.fr%2Ffull_width%2F808497%2Fpeek.jpg&imgrefurl=https%3A%2F%2Fwww.latribune.fr%2Fentreprises-finance%2Findustrie%2Fagroalimentaire-biens-de-consommation-luxe%2Fspace-de-rennes-les-agriculteurs-invites-a-se-connecter-au-numerique-750452.html&docid=H0IAhTfMv96W1M&tbnid=S_b8qPoa2gCWRM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhUKAwwDA..i&w=612&h=306&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhUKAwwDA&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.theconversation.com%2Ffiles%2F36814%2Foriginal%2Ffgrdh3fb-1386080779.jpg%3Fixlib%3Drb-1.1.0%26q%3D45%26auto%3Dformat%26w%3D926%26fit%3Dclip&imgrefurl=http%3A%2F%2Ftheconversation.com%2Fspace-exploration-can-drive-the-next-agricultural-revolution-21095&docid=aks1H93jiqIJFM&tbnid=UeHQtOyNQ5QvTM%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwh2KCAwIA..i&w=926&h=403&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwh2KCAwIA&iact=mrc&uact=8
%https://www.google.com/imgres?imgurl=http%3A%2F%2Fimages.techtimes.com%2Fdata%2Fimages%2Ffull%2F330447%2Fcrops-in-space.jpg&imgrefurl=https%3A%2F%2Fwire.thearabianpost.com%2Fspace-agriculture-dwarf-wheat-grows-at-the-iss-with-advanced-plant-habitat.aspx&docid=6G6nEJwtPivseM&tbnid=P3tZmn5pLmA36M%3A&vet=10ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhzKB0wHQ..i&w=1200&h=800&client=ubuntu&bih=980&biw=1855&q=space%20agriculture&ved=0ahUKEwj0tsnrjablAhXlD2MBHUafBsAQMwhzKB0wHQ&iact=mrc&uact=8
%https://www.google.com/search?client=ubuntu&hs=2JY&channel=fs&ei=AuipXcuBM4fmU8z0gugJ&q=colorado+university+boulder+space+gardening&oq=colorado+university+boulder+space+gardening&gs_l=psy-ab.3...20600.23721..23799...0.0..0.103.1409.17j2......0....1..gws-wiz.......33i10.9lOWnscX4fk&ved=0ahUKEwjLitWDnablAhUH8xQKHUy6AJ0Q4dUDCAo&uact=5
%https://www.google.com/search?q=colorado+space+gardening&client=ubuntu&hs=2JY&channel=fs&tbm=isch&source=iu&ictx=1&fir=EPbxGrZrQlkU3M%253A%252Cc3w1-Lu0IQ3dPM%252C_&vet=1&usg=AI4_-kTerOQlK7QsuvP388j8Htz0Qp5ETg&sa=X&ved=2ahUKEwjLitWDnablAhUH8xQKHUy6AJ0Q9QEwAHoECAQQBg#imgrc=EPbxGrZrQlkU3M:
%https://www.youtube.com/watch?v=M7LslyCX7Jg
%https://www.youtube.com/watch?v=eiALwiNuLBg
%https://www.youtube.com/watch?v=Z_ZgZtXK438
%https://www.youtube.com/watch?v=YW-mTIywbQ0
%https://www.degruyter.com/downloadpdf/j/opag.2017.2.issue-1/opag-2017-0002/opag-2017-0002.pdf
%https://pdfs.semanticscholar.org/3eec/821ca0cc1cb8fd8219ee03305bf77fee5246.pdf
%https://gastropod.com/potatoes-in-space/
%https://scholar.google.com/scholar?hl=fr&as_sdt=0%2C5&q=Raymond+M.+Wheeler+&btnG=
%https://www.nasa.gov/image-feature/growing-veggies-in-space
%\url{https://www.youtube.com/watch?v=YW-mTIywbQ0}
%\end{abstract}

